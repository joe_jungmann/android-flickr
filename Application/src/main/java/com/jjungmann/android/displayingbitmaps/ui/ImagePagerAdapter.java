package com.jjungmann.android.displayingbitmaps.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.modulews.models.FlickrFeed;


/**
 * The main adapter that backs the ViewPager. A subclass of FragmentStatePagerAdapter as there
 * could be a large number of items in the ViewPager and we don't want to retain them all in
 * memory at once but create/destroy them on the fly.
 */
public class ImagePagerAdapter extends FragmentStatePagerAdapter {

    private FlickrFeed flickrFeed;

    public ImagePagerAdapter(FragmentManager fm) {
        super(fm);
        flickrFeed = new FlickrFeed();
    }

    @Override
    public int getCount() {
        return flickrFeed.size();
    }

    @Override
    public int getItemPosition(Object object) {

        return -2;
    }

    @Override
    public Fragment getItem(int position) {

        return ImageDetailFragment.newInstance(flickrFeed.getItem(position));
    }

    public void setFlickrFeed(FlickrFeed flickrFeed) {
        this.flickrFeed = flickrFeed;
        notifyDataSetChanged();
    }
}
