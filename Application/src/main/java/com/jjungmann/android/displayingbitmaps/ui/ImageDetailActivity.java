/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jjungmann.android.displayingbitmaps.ui;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;

import com.example.modulews.models.FlickrFeed;
import com.example.modulews.ServiceBuilder;
import com.example.modulews.ServiceCalls;
import com.google.gson.Gson;
import com.jjungmann.android.displayingbitmaps.BuildConfig;
import com.jjungmann.android.displayingbitmaps.R;
import com.jjungmann.android.displayingbitmaps.util.ImageCache;
import com.jjungmann.android.displayingbitmaps.util.ImageFetcher;
import com.jjungmann.android.displayingbitmaps.util.Utils;

import butterknife.Bind;
import butterknife.ButterKnife;
import github.chenupt.springindicator.SpringIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ImageDetailActivity extends FragmentActivity implements OnClickListener, SwipeRefreshLayout.OnRefreshListener, ViewPager.OnPageChangeListener {

    private static final String IMAGE_CACHE_DIR = "images";
    public static final String EXTRA_IMAGE = "extra_image";

    public static FlickrFeed flickrFeed;

    private ImagePagerAdapter mAdapter;
    private ImageFetcher mImageFetcher;


    @Bind(R.id.pager)
    ViewPager mPager;

    @Bind(R.id.swipe_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    SpringIndicator indicator;

    Call<FlickrFeed> getFeed;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        if (BuildConfig.DEBUG) {
            Utils.enableStrictMode();
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_detail_pager);
        ButterKnife.bind(this);

        Retrofit builder = ServiceBuilder.getMainService();

        getFeed = builder
                .create(ServiceCalls.class)
                .getPhotos();


        // Fetch screen height and width, to use as our max size when loading images as this
        // activity runs full screen
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        final int height = displayMetrics.heightPixels;
        final int width = displayMetrics.widthPixels;

        // For this sample we'll use half of the longest width to resize our images. As the
        // image scaling ensures the image is larger than this, we should be left with a
        // resolution that is appropriate for both portrait and landscape.
        final int longest = (height > width ? height : width);

        ImageCache.ImageCacheParams cacheParams =
                new ImageCache.ImageCacheParams(this, IMAGE_CACHE_DIR);
        cacheParams.setMemCacheSizePercent(0.35f); // Set memory cache to 35% of app memory

        // The ImageFetcher takes care of loading images into our ImageView children asynchronously
        mImageFetcher = new ImageFetcher(this, longest);
        mImageFetcher.addImageCache(getSupportFragmentManager(), cacheParams);
        mImageFetcher.setImageFadeIn(false);

        mPager = (ViewPager) findViewById(R.id.pager);
        mAdapter = new ImagePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mAdapter);


        if (savedInstanceState != null) {
            flickrFeed = new Gson().fromJson(savedInstanceState.getString("saved_data"), FlickrFeed.class);
            mAdapter.setFlickrFeed(flickrFeed);
            setIndicator();
        } else {
            fetchData();
        }

        swipeRefreshLayout.setOnRefreshListener(this);

        // Set up ViewPager and backing adapter
        mPager.setPageMargin((int) getResources().getDimension(R.dimen.horizontal_page_margin));
        mPager.setOffscreenPageLimit(2);
        mPager.addOnPageChangeListener(this);

        final ActionBar actionBar = getActionBar();

        // Hide title text and set home as up
        actionBar.setDisplayShowTitleEnabled(false);

        // Hide and show the ActionBar as the visibility changes
        mPager.setOnSystemUiVisibilityChangeListener(
                new View.OnSystemUiVisibilityChangeListener() {
                    @Override
                    public void onSystemUiVisibilityChange(int vis) {
                        if ((vis & View.SYSTEM_UI_FLAG_LOW_PROFILE) != 0) {
                            actionBar.hide();
                        } else {
                            actionBar.show();
                        }
                    }
                });

        // Start low profile mode and hide ActionBar
        mPager.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);


        actionBar.hide();


        // Set the current item based on the extra passed in to this activity
        final int extraCurrentItem = getIntent().getIntExtra(EXTRA_IMAGE, -1);
        if (extraCurrentItem != -1) {
            mPager.setCurrentItem(extraCurrentItem);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (flickrFeed != null && flickrFeed.size() > 0) {
            outState.putString("saved_data", new Gson().toJson(flickrFeed));
        }

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mImageFetcher.setExitTasksEarly(false);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mImageFetcher.setExitTasksEarly(true);
        mImageFetcher.flushCache();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getFeed.cancel();
        mImageFetcher.closeCache();
    }

    /**
     * Called by the ViewPager child fragments to load images via the one ImageFetcher
     */
    public ImageFetcher getImageFetcher() {
        return mImageFetcher;
    }

    public void onPostExecute(FlickrFeed feed) {
        swipeRefreshLayout.setRefreshing(false);

        if(feed != null){
            flickrFeed = feed;
        }

        if (mAdapter != null) {
            mAdapter.setFlickrFeed(flickrFeed);
            mPager.setCurrentItem(0);
        }

        setIndicator();
    }

    private void setIndicator(){

        // Indicator requires a minimum feedsize of 1.
        if (indicator != null || flickrFeed == null || flickrFeed.size() <= 0) {
            return;
        }

        indicator = ((SpringIndicator) findViewById(R.id.indicator));
        indicator.setViewPager(mPager);
        indicator.setVisibility(View.VISIBLE);
    }

    /**
     * Set on the ImageView in the ViewPager children fragments, to enable/disable low profile mode
     * when the ImageView is touched.
     */
    @Override
    public void onClick(View v) {
        final int vis = mPager.getSystemUiVisibility();
        if ((vis & View.SYSTEM_UI_FLAG_LOW_PROFILE) != 0) {
            mPager.setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
        } else {
            mPager.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
        }
    }

    private void fetchData(){
        getFeed
                .clone()
                .enqueue(new Callback<FlickrFeed>() {

                    @Override
                    public void onResponse(Response<FlickrFeed> response) {
                        onPostExecute(response.body());
                    }

                    @Override
                    public void onFailure(Throwable t) {

                    }
                });
    }

    @Override
    public void onRefresh() {
        fetchData();
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        swipeRefreshLayout.setEnabled(state == ViewPager.SCROLL_STATE_IDLE);
    }

    @Override
    public void onPageScrolled(int position, float v, int i1) {}

    @Override
    public void onPageSelected(int position) {}
}
