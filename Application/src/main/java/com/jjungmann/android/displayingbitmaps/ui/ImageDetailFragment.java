/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jjungmann.android.displayingbitmaps.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.example.modulews.models.FlickrItem;
import com.jjungmann.android.displayingbitmaps.R;
import com.jjungmann.android.displayingbitmaps.util.ImageFetcher;
import com.jjungmann.android.displayingbitmaps.util.ImageWorker;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * This fragment will populate the children of the ViewPager from {@link ImageDetailActivity}.
 */
public class ImageDetailFragment extends Fragment implements ImageWorker.OnImageLoadedListener {
    private static final String IMAGE_TITLE_EXTRA = "extra_title_data";
    private static final String IMAGE_DATA_EXTRA = "extra_image_data";

    private String mImageUrl;
    private String mImageTitle;

    @Bind(R.id.imageView)
    ImageView mImageView;

    @Bind(R.id.imageTitle)
    TextView mTextView;

    @Bind(R.id.progressbar)
    ProgressBar mProgressBar;

    private ImageFetcher mImageFetcher;


    /**
     * Factory method to generate a new instance of the fragment given an image number.
     *
     * @param item The image url to load
     * @return A new instance of ImageDetailFragment with imageNum extras
     */
    public static ImageDetailFragment newInstance(FlickrItem item) {
        final ImageDetailFragment frag = new ImageDetailFragment();

        final Bundle args = new Bundle();

        // only going to add these two properties rather than boxing and unboxing the entire FlickrItem
        args.putString(IMAGE_TITLE_EXTRA, item.getTitle());
        args.putString(IMAGE_DATA_EXTRA, item.getMedia());
        frag.setArguments(args);

        return frag;
    }

    /**
     * Empty constructor as per the Fragment documentation
     */
    public ImageDetailFragment() {}

    /**
     * Populate image using a url from extras, use the convenience factory method
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mImageUrl = getArguments() != null ? getArguments().getString(IMAGE_DATA_EXTRA) : null;
        mImageTitle = getArguments() != null ? getArguments().getString(IMAGE_TITLE_EXTRA) : null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate and locate the main ImageView
        final View v = inflater.inflate(R.layout.image_detail_fragment, container, false);
        ButterKnife.bind(this, v);

        mTextView.setText(mImageTitle);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mImageView.setOnClickListener((OnClickListener) getActivity());
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.d("ActivityResumed","Resume");
        mImageFetcher = ((ImageDetailActivity) getActivity()).getImageFetcher();
        mImageFetcher.loadImage(mImageUrl, mImageView, this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("onDestroy","Destruction");
        if (mImageView != null) {
            // Cancel any pending image work
            ImageWorker.cancelWork(mImageView);
            mImageView.setImageDrawable(null);
        }
    }

    @Override
    public void onImageLoaded(boolean success) {
        Log.d("onImageLoaded", String.valueOf(success));
        // Set loading spinner to gone once image has loaded. Cloud also show
        // an error view here if needed.
        mProgressBar.setVisibility(View.GONE);

    }
}
