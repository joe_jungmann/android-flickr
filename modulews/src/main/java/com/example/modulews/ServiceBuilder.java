package com.example.modulews;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;
public class ServiceBuilder {

    private static Retrofit webService = null;

    //the standard builder to use once in the app
    public static Retrofit getMainService() {
        if (webService==null) {
            OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
            builder.readTimeout(20, TimeUnit.SECONDS);
            builder.connectTimeout(10, TimeUnit.SECONDS);
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
            OkHttpClient client = builder.build();
            String webserviceEndpoint = "http://api.flickr.com/services/";
            webService = new Retrofit.Builder()
                    .baseUrl(webserviceEndpoint)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return webService;
    }

}



