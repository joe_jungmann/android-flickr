package com.example.modulews.models;

/**
 * Created by jjungmann on 3/11/16.
 */
@SuppressWarnings("SpellCheckingInspection")
public class FlickrItem {
    String title;
    String link;
    String date_taken;
    String description;
    String published;
    String author;
    String author_id;
    String tags;
    FlickrMedia media;

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the link
     */
    public String getLink() {
        return link;
    }

    /**
     * @param link the link to set
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     * @return the dateTaken
     */
    public String getDateTaken() {
        return date_taken;
    }

    /**
     * @param dateTaken the dateTaken to set
     */
    public void setDateTaken(String dateTaken) {
        this.date_taken = dateTaken;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the published
     */
    public String getPublished() {
        return published;
    }

    /**
     * @param published the published to set
     */
    public void setPublished(String published) {
        this.published = published;
    }

    /**
     * @return the author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * @param author the author to set
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * @return the authorId
     */
    public String getAuthorId() {
        return author_id;
    }

    /**
     * @param authorId the authorId to set
     */
    public void setAuthorId(String authorId) {
        this.author_id = authorId;
    }

    /**
     * @return the tags
     */
    public String getTags() {
        return tags;
    }

    /**
     * @param tags the tags to set
     */
    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getMedia() {
        return media.getPath();
    }
}
