package com.example.modulews.models;

import java.util.List;

/**
 * Created by jjungmann on 3/11/16.
 */

@SuppressWarnings("SpellCheckingInspection")
public class FlickrFeed {
    String title;
    String link;
    String description;
    String modified;
    String generator;
    List<FlickrItem> items;
    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }
    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
    /**
     * @return the link
     */
    public String getLink() {
        return link;
    }
    /**
     * @param link the link to set
     */
    public void setLink(String link) {
        this.link = link;
    }
    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }
    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    /**
     * @return the modified
     */
    public String getModified() {
        return modified;
    }
    /**
     * @param modified the modified to set
     */
    public void setModified(String modified) {
        this.modified = modified;
    }
    /**
     * @return the generator
     */
    public String getGenerator() {
        return generator;
    }
    /**
     * @param generator the generator to set
     */
    public void setGenerator(String generator) {
        this.generator = generator;
    }
    /**
     * @return the items
     */
    public List<FlickrItem> getItems() {
        return items;
    }
    /**
     * @param items the items to set
     */
    public void setItems(List<FlickrItem> items) {
        this.items = items;
    }


    public int size(){
        if(this.items != null){
            return this.items.size();
        }

        return 0;
    }

    public FlickrItem getItem(int index){
        return items.get(index);
    }

    public String getMedia(int index){
        return items.get(index).getMedia();
    }
}
