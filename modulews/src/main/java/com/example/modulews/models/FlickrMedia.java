package com.example.modulews.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jjungmann on 3/11/16.
 */
@SuppressWarnings("SpellCheckingInspection")
public class FlickrMedia {

    @SerializedName("m")
    String path;

    /**
     * @return the media
     */
    public String getPath() {
        return path.replace("_m.jpg","_z.jpg");
    }
}
