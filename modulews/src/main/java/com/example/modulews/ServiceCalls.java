package com.example.modulews;

import com.example.modulews.models.FlickrFeed;
import com.google.gson.JsonObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ServiceCalls {

    @POST("feeds/photos_public.gne?format=json&nojsoncallback=1&id=92253640%40N03")
    Call<FlickrFeed> getPhotos();
}
